(function ($, Drupal) {
    'use strict';

    Drupal.behaviors.siteBannerBehavior = {
      attach: function () {
        if ($.cookie('acknowledged_site_banner') === null || typeof $.cookie('acknowledged_site_banner') === 'undefined') {
          $('#site-banner-header-banner').css('display', 'block');
          $('#site-banner-footer-banner').css('display', 'block');
        }

        $("#site-banner-header-banner .close").click(function() {
          $.cookie('acknowledged_site_banner', true, { expires: 1, path: '/' });
          $('#site-banner-header-banner').css('display', 'none');
          $('#site-banner-footer-banner').css('display', 'none');
        });
      }
    };
  }
)(jQuery, Drupal);
