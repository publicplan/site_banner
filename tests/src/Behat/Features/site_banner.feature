@api @drupal
Feature: site banner

  Background:
    Given I proof that Drupal module "site_banner" is installed
    Given I proof that Drupal module "dynamic_page_cache" is installed
    Given I proof that Drupal module "page_cache" is not installed

  Scenario: Check if banner will be shown
    Given I am logged in as a user with the "administrator" role
    And I am on "/admin/config/site_banner/configuration"
    And I check the box "edit-status"
    And I check the box "edit-show-header"
    And I select the radio button "Success"
    And I fill in "Test12345" for "edit-site-banner-text"
    And I fill in "00002001" for "site_banner_start_date[date]"
    And I fill in "000000AM" for "site_banner_start_date[time]"
    And I fill in "00002050" for "site_banner_end_date[date]"
    And I fill in "000000AM" for "site_banner_end_date[time]"
    And I press the "edit-submit" button
    Then I should see text matching "The configuration options have been saved."
    And I am on "/"
    Then I should see text matching "Test12345"
    And I should see HTML content matching "alert alert-dismissible alert-success"
