<?php
// This file is for Bitbucket pipelines.

$file = 'composer.json';
$data = json_decode(file_get_contents($file), true);
$data["autoload-dev"]["psr-4"] = array("Drupal\\Tests\\site_banner\\Behat\\Context\\" => "web/modules/contrib/site_banner/tests/src/Behat/Context");
file_put_contents('composer.json', json_encode($data, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT));
