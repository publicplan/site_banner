<?php

namespace Drupal\Tests\site_banner\Behat\Context;

use Behat\Mink\Exception\ResponseTextException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\DrupalExtension\Context\RawDrupalContext;

class SiteBannerContext extends RawDrupalContext {

  /**
   * @When I fill date field with :time seconds in future
   */
  public function fillDateFieldWithSecondsInFuture($seconds): void {
    $drupalDateTime = new DrupalDateTime('now');
    $drupalDateTime->modify('+' . $seconds . ' seconds');

    $this->getSession()
      ->getPage()
      ->fillField('site_banner_end_date[date]', $drupalDateTime->format('dmY'));
    $this->getSession()
      ->getPage()
      ->fillField('site_banner_end_date[time]', $drupalDateTime->format('HisA'));
  }

  /**
   * @Then /^I should see HTML content matching "([^"]*)"$/
   */
  public function iShouldSeeHTMLContent($html) {
    $content = $this->getSession()->getPage()->getText();
    if (substr_count($content, $html) !== 0) {
      return TRUE;
    }
  }

  /**
   * @Then /^wait (\d+) seconds$/
   */
  public function waitSeconds($secondsNumber) {
    $this->getSession()->wait($secondsNumber * 1000);
  }

  /**
   * @Then /^I proof that Drupal module "([^"]*)" is installed$/
   */
  public function proofDrupalModuleIsInstalled($moduleName): void {
    if (!$this->getModuleHandler()->moduleExists($moduleName)){
      throw new ResponseTextException("Drupal module $moduleName is not installed.", $this->getSession());
    }
  }

  /**
   * @Then /^I proof that Drupal module "([^"]*)" is not installed$/
   */
  public function proofDrupalModuleIsNotInstalled($moduleName): void {
    if ($this->getModuleHandler()->moduleExists($moduleName)){
      throw new ResponseTextException("Drupal module $moduleName is installed.", $this->getSession());
    }
  }

  protected function getModuleHandler(): ModuleHandler {
    return \Drupal::service('module_handler');
  }

  /**
   * Dumps the current page HTML.
   *
   * @When I dump the HTML
   */
  public function dumpHTML() {
    print_r($this->getSession()->getPage()->getContent());
  }

}
