<?php

namespace Drupal\Tests\site_banner\Unit;

use Drupal\site_banner\DateRangeValidator;
use Drupal\Tests\UnitTestCase;

class DateRangeValidatorTest extends UnitTestCase {

  public function setUp() {
    parent::setUp();
  }

  public function testAreDatesInCurrentDateRange() {
    $dateRangeValidator = new DateRangeValidator();

    self::assertTrue($dateRangeValidator->areDatesInCurrentDateRange('2000-01-01T00:00:00', '2099-11-25T18:00:00', 'Europe/Berlin'));
  }

  public function testAreDatesNotInCurrentDateRange() {
    $dateRangeValidator = new DateRangeValidator();
    self::assertFalse($dateRangeValidator->areDatesInCurrentDateRange('2000-01-01T00:00:00', '2018-09-25T14:00:00', 'Europe/Berlin'));
  }

}
