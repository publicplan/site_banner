<?php

namespace Drupal\Tests\site_banner\Unit;

use Consolidation\Config\ConfigInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Element\Date;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\site_banner\DateRangeValidator;
use Drupal\site_banner\RenderArrayCreator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Routing\Route;

class RenderArrayCreatorTest extends UnitTestCase {

  /**
   * @var ConfigFactoryInterface
   */
  private $configFactoryMock;
  /**
   * @var CurrentRouteMatch
   */
  private $currentRouteMatchMock;
  /**
   * @var AdminContext
   */
  private $adminContextMock;

  public function setUp() {
    parent::setUp();

    $configMapSiteBannerSettings = [
      ['site_banner_text', 'Some site banner text'],
      ['status', TRUE],
      ['show_header', TRUE],
      ['show_footer', TRUE],
      ['site_banner_start_date', '2000-01-01T00:00:00'],
      ['site_banner_end_date', '2199-01-19T19:33:00'],
      ['alert_type', 'notice'],
    ];

    /**
     * @var ConfigInterface $configMockSiteBannerSettings
     */
    $configMockSiteBannerSettings = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->setMethods(['get'])
      ->getMock();

    $configMockSiteBannerSettings->method('get')
      ->will($this->returnValueMap($configMapSiteBannerSettings));

    /**
     * @var ConfigInterface $configMockSiteBannerSettings
     */
    $configMockSystemDate = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->setMethods(['get'])
      ->getMock();

    $configMockSystemDate->method('get')
      ->willReturn('Europe/Berlin');

    $configFactoryMap = [
      ['site_banner.settings', $configMockSiteBannerSettings],
      ['system.date', $configMockSystemDate],
    ];

    /**
     * @var ConfigFactoryInterface $configFactoryMock
     */
    $this->configFactoryMock = $this->createMock(ConfigFactoryInterface::class);
    $this->configFactoryMock
      ->method('get')
      ->will($this->returnValueMap($configFactoryMap));

    $route = new Route('some.route.stub');

    $this->currentRouteMatchMock = $this->createMock(CurrentRouteMatch::class);
    $this->currentRouteMatchMock
      ->method('getRouteObject')
      ->willReturn($route);

    /**
     * @var AdminContext $adminContextMock
     */
    $this->adminContextMock = $this->createMock(AdminContext::class);
    $this->adminContextMock
      ->method('isAdminRoute')
      ->willReturn(FALSE);
  }

  public function testComputeRenderArray(): void {
    $dateRangeValidator = new DateRangeValidator();
    $renderArrayCreator = new RenderArrayCreator($this->configFactoryMock, $this->currentRouteMatchMock, $this->adminContextMock, $dateRangeValidator);
    $pageTop = $renderArrayCreator->create([]);

    $expectedPageTop = array(
      'site_banner' =>
        array (
          'header' =>
            array (
              '#theme' => 'site_banner',
              '#content' => 'Some site banner text',
              '#alert_type' => 'notice',
              '#is_footer' => false,
              '#cache' =>
                array (
                  'max-age' => 0,
                  'tags' =>
                    array (
                      0 => 'config:site_banner.settings',
                    ),
                ),
            ),
          'footer' =>
            array (
              '#theme' => 'site_banner',
              '#content' => 'Some site banner text',
              '#alert_type' => 'notice',
              '#is_footer' => true,
              '#cache' =>
                array (
                  'max-age' => 0,
                  'tags' =>
                    array (
                      0 => 'config:site_banner.settings',
                    ),
                ),
            ),
        ),
    );

    self::assertEquals($expectedPageTop, $pageTop);
  }

  public function testComputeRenderArrayOnPastEnddate(): void {
    $configMapSiteBannerSettings = [
      ['site_banner_text', 'Some site banner text'],
      ['status', TRUE],
      ['show_header', TRUE],
      ['show_footer', TRUE],
      ['site_banner_start_date', '2000-01-01T00:00:00'],
      ['site_banner_end_date', '2018-01-19T19:33:00'],
      ['alert_type', 'notice'],
    ];

    /**
     * @var ConfigInterface $configMockSiteBannerSettings
     */
    $configMockSiteBannerSettings = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->setMethods(['get'])
      ->getMock();

    $configMockSiteBannerSettings->method('get')
      ->will($this->returnValueMap($configMapSiteBannerSettings));

    /**
     * @var ConfigInterface $configMockSiteBannerSettings
     */
    $configMockSystemDate = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->setMethods(['get'])
      ->getMock();

    $configMockSystemDate->method('get')
      ->willReturn('Europe/Berlin');

    $configFactoryMap = [
      ['site_banner.settings', $configMockSiteBannerSettings],
      ['system.date', $configMockSystemDate],
    ];

    /**
     * @var ConfigFactoryInterface $configFactoryMock
     */
    $this->configFactoryMock = $this->createMock(ConfigFactoryInterface::class);
    $this->configFactoryMock
      ->method('get')
      ->will($this->returnValueMap($configFactoryMap));

    $dateRangeValidator = new DateRangeValidator();
    $renderArrayCreator = new RenderArrayCreator($this->configFactoryMock, $this->currentRouteMatchMock, $this->adminContextMock, $dateRangeValidator);
    $pageTop = $renderArrayCreator->create([]);

    self::assertEquals(NULL, $pageTop);
  }

}
