<?php

namespace Drupal\site_banner;

class DateRangeValidator {

  public function areDatesInCurrentDateRange($startDrupalDatetime, $endDrupalDatetime, string $timezone): bool {
    $dateTime = new \DateTime('now', new \DateTimeZone($timezone));
    $currentTimestamp = $dateTime->getTimestamp();

    // If startDrupalDatetime is a string check that we have passed it.
    if(\is_string($startDrupalDatetime)) {
      $startTimestamp = $this->computeDrupalDatetimeToTimestamp($startDrupalDatetime, $timezone);
      if ($startTimestamp > $currentTimestamp) {
        return FALSE;
      }
    }

    // If endDrupalDatetime is a string check that we have not passed it.
    if(\is_string($endDrupalDatetime)) {
      $endTimestamp = $this->computeDrupalDatetimeToTimestamp($endDrupalDatetime, $timezone);
      if ($endTimestamp < $currentTimestamp) {
        return FALSE;
      }
    }

    return TRUE;
  }

  private function computeDrupalDatetimeToTimestamp(string $drupalDatetime, string $timezone): int {
    if (ctype_digit($drupalDatetime)) {
      throw new \Exception('Passed variable does not contain the Drupal date format. Looks like a timestamp.');
    }
    $dateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $drupalDatetime, new \DateTimeZone($timezone));

    return $dateTime->getTimestamp();
  }

}
