<?php

namespace Drupal\site_banner\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class SiteBannerConfigurationForm.
 */
class SiteBannerConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_banner.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_banner_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (strip_tags($form_state->getValue('site_banner_text')) !== $form_state->getValue('site_banner_text')) {
      $form_state->setErrorByName('site_banner_text', $this->t('Only plain text is allowed. You must remove any HTML.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_banner.settings');
    $startDate = NULL;
    $endDate = NULL;

    if ($config->get('site_banner_start_date')) {
      $startDate = new DrupalDateTime ($config->get('site_banner_start_date'));
    }

    if ($config->get('site_banner_end_date')) {
      $endDate = new DrupalDateTime ($config->get('site_banner_end_date'));
    }

    $form['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Status'),
      '#description'   => $this->t('Enable/disable banner. Uncheck to clear the date fields.'),
      '#default_value' => $config->get('status'),
    ];

    $form['show_header'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Header banner'),
      '#description'   => $this->t('Enable/disable banner in the header region.'),
      '#default_value' => $config->get('show_header'),
    ];


    $form['show_footer'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Footer banner'),
      '#description'   => $this->t('Enable/disable banner in the footer region.'),
      '#default_value' => $config->get('show_footer'),
    ];

    $form['site_banner_text'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Site banner text'),
      '#description'   => $this->t('The text that will be displayed in the banner. Only plain text is allowed.'),
      '#default_value' => $config->get('site_banner_text'),
      '#required'      => TRUE,
    ];

    $form['alert_type'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Site banner alert type'),
      '#description'   => $this->t(
        'Per default Bootstrap 4 alert types are used. See @url.',
        [
          '@url' => Link::fromTextAndUrl(
            'https://getbootstrap.com/docs/4.0/components/alerts',
            Url::fromUri('https://getbootstrap.com/docs/4.0/components/alerts', ['attributes' => ['target' => '_blank']]))
            ->toString(),
        ]
      ),
      '#default_value' => $config->get('alert_type'),
      '#options'       => [
        'alert-success' => $this->t('Success'),
        'alert-warning' => $this->t('Warning'),
        'alert-danger'  => $this->t('Danger'),
        'alert-info'    => $this->t('Info'),
      ],
      '#required'      => TRUE,
    ];

    $form['site_banner_start_date'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Show banner at specific date'),
      '#description'   => $this->t('If no date is specified, the banner will be shown immediately.'),
      '#default_value' => $startDate,
    ];

    $form['site_banner_end_date'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Hide banner at specific date'),
      '#description'   => $this->t('If no date is specified, the banner will be shown indefinitely.'),
      '#default_value' => $endDate,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->configFactory->getEditable('site_banner.settings');

    $config->set('site_banner_text', $form_state->getValue('site_banner_text'));
    $config->set('status', $form_state->getValue('status'));
    $config->set('show_footer', $form_state->getValue('show_footer'));
    $config->set('show_header', $form_state->getValue('show_header'));
    $config->set('site_banner_color', $form_state->getValue('site_banner_color'));
    $config->set('alert_type', $form_state->getValue('alert_type'));

    if (!$form_state->getValue('status')) {
      $config->set('site_banner_start_date', NULL);
      $config->set('site_banner_end_date', NULL);
      $config->save();

      return;
    }

    if ($form_state->getValue('site_banner_start_date')) {
      $config->set('site_banner_start_date', $form_state->getValue('site_banner_start_date')
        ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
    }

    if ($form_state->getValue('site_banner_end_date')) {
      $config->set('site_banner_end_date', $form_state->getValue('site_banner_end_date')
        ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
    }

    $config->save();

    Cache::invalidateTags(['config:site_banner.settings']);
  }

}
