<?php

namespace Drupal\site_banner;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\CurrentRouteMatch;

class RenderArrayCreator {

  /**
   * @var ConfigFactoryInterface
   */
  private $configFactory;
  /**
   * @var CurrentRouteMatch
   */
  private $currentRouteMatch;
  /**
   * @var AdminContext
   */
  private $adminContext;
  /**
   * @var DateRangeValidator
   */
  private $dateRangeValidator;

  public function __construct(ConfigFactoryInterface $configFactory, CurrentRouteMatch $currentRouteMatch, AdminContext $adminContext, DateRangeValidator $dateRangeValidator)
  {
    $this->configFactory = $configFactory;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->adminContext = $adminContext;
    $this->dateRangeValidator = $dateRangeValidator;
  }

  public function create(): ?array {
    $pageTop = [];
    $config = $this->configFactory->get('site_banner.settings');

    if ($this->adminContext->isAdminRoute($this->currentRouteMatch->getRouteObject())) {
      return null;
    }

    $status = $config->get('status') ?? FALSE;
    if (!$status) {
      return null;
    }

    $banner_text = $config->get('site_banner_text');

    $header = $config->get('show_header') ?? FALSE;
    $footer = $config->get('show_footer') ?? FALSE;

    if (empty($status) || empty($banner_text) || (empty($header) && empty($footer))) {
      throw new \Exception('Missing required config values');
    }

    if (!$this->dateRangeValidator->areDatesInCurrentDateRange($config->get('site_banner_start_date'), $config->get('site_banner_end_date'), $this->configFactory->get('system.date')->get('timezone.default'))) {
      return null;
    }

    if ($header) {
      $pageTop['site_banner']['header'] = [
        '#theme'      => 'site_banner',
        '#content'    => $banner_text,
        '#alert_type' => $config->get('alert_type'),
        '#is_footer'  => FALSE,
        '#cache' => [
          'max-age' => 0,
          'tags' => ['config:site_banner.settings']
        ]
      ];
    }

    if ($footer) {
      $pageTop['site_banner']['footer'] = [
        '#theme'      => 'site_banner',
        '#content'    => $banner_text,
        '#alert_type' => $config->get('alert_type'),
        '#is_footer'  => TRUE,
        '#cache' => [
          'max-age' => 0,
          'tags' => ['config:site_banner.settings']
        ]
      ];
    }

    return $pageTop;
  }

}
